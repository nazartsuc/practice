package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        info = findViewById(R.id.info);

        String key = "8906ab15960762c0faad5e1e712166b0";
        String url = "https://api.openweathermap.org/data/2.5/forecast?q=Kremenchug&cnt=4&appid=" + key + "&units=metric&lang=ru";

        new GetURLData().execute(url);
    }

    private class GetURLData extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            info.setText("Ожидайте...");
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null)
                    buffer.append(line).append("\n");

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(connection != null)
                    connection.disconnect();

                try {
                    if (reader != null)
                        reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                String str1 = jsonObject.getJSONArray("list").getJSONObject(0).getJSONArray("weather").getJSONObject(0).getString("description");
                str1 += "\nТемпература: " + jsonObject.getJSONArray("list").getJSONObject(0).getJSONObject("main").getInt("temp");
                String str2 = jsonObject.getJSONArray("list").getJSONObject(1).getJSONArray("weather").getJSONObject(0).getString("description");
                str2 += "\nТемпература: " + jsonObject.getJSONArray("list").getJSONObject(1).getJSONObject("main").getInt("temp");
                String str3 = jsonObject.getJSONArray("list").getJSONObject(2).getJSONArray("weather").getJSONObject(0).getString("description");
                str3 += "\nТемпература: " + jsonObject.getJSONArray("list").getJSONObject(2).getJSONObject("main").getInt("temp");
                String str4 = jsonObject.getJSONArray("list").getJSONObject(3).getJSONArray("weather").getJSONObject(0).getString("description");
                str4 += "\nТемпература: " + jsonObject.getJSONArray("list").getJSONObject(3).getJSONObject("main").getInt("temp");
                info.setText("Сегодня: " + str1 + "\n\nЗавтра: " + str2 + "\n\nПослезавтра: " + str3 + "\n\nСледующий день: " + str4);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}